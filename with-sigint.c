/*
 * with-sigint.c
 *
 * A tool that un-ignores the SIGINT signal and then executes the
 * thing passed as its arguments.
 *
 * Originally written to help with PyCharm that has been launched from
 * dmenu.
 */

#include <stdio.h>
#include <stdlib.h>

#include <signal.h>
#include <unistd.h>

int main(int argc, char ** argv)
{
	struct sigaction const action = {
		.sa_handler = SIG_DFL,
	};

	int const sa_rv = sigaction(
		SIGINT,
		&action,
		0
	);

	if (sa_rv < 0) {
		perror("Failed to set sigaction");
		return EXIT_FAILURE;
	}

	char ** argv_next = argv + 1;

	int const execvp_rv = execvp(
		argv_next[0],
		argv_next
	);

	if (execvp_rv < 0) {
		perror("Failed to exec");
	} else {
		fprintf(stderr, "This is not expected to happen\n");
	}

	return EXIT_FAILURE;
}
